﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpilasteNimekiri
{
    class Inimene
    {
        public string Nimi;
        public string Isikukood;
        public DateTime Sünniaeg()
        {
            int sajand = 1800; // miks siia 1800?
            switch (Isikukood.Substring(0, 1))
            {
                case "3": case "4": sajand = 1900; break;
                case "5": case "6": sajand = 2000; break;
            }

            return new DateTime(
                int.Parse(Isikukood.Substring(1, 2)), // siia aasta
                int.Parse(Isikukood.Substring(3, 2)), // siia kuu
                int.Parse(Isikukood.Substring(5, 2))  // siia päev

                );
        }
        public int Vanus() => (DateTime.Today - Sünniaeg()).Days * 4 / 1461;
        public override string ToString()
        => $"{Isikukood} - {Nimi}";
                };
        

    }

