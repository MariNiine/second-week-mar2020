﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OpilasteNimekiri
{
    class Program
    {
        static void Main(string[] args)

        {
            List<Inimene> õpilased = new List<Inimene>();
            foreach (var rida in System.IO.File.ReadAllLines(@"C:\Users\marin_m3sdpn5\Source\Repos\second-week-mar2020\OpilasteNimekiri\Opilased.txt"))
            {
                var osad = rida.Replace(", ", ",").Split(',');
                if (osad.Length > 1) õpilased.Add(new Inimene{Nimi = osad[1], Isikukood = osad[0]});
            }

            foreach (var i in õpilased) Console.WriteLine($"{i} vanus on {i.Vanus()}");
        }
    }
}





