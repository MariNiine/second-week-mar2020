﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordipaevTeisiti
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Loeme sisse protokolli, kui ei õnnestu siis...
            string filename2 = "Sport.txt";
            string folder = ".";
            string[] failisisu;
            while (true)
            {
                try
                {
                    failisisu = File.ReadAllLines($"{folder}\\{filename2}");
                    break;
                }
                catch (Exception e)
                {
                    Console.Write("Kus su fail on: ");
                    folder = Console.ReadLine();
                }
            } 
            #endregion
            //   Console.WriteLine(string.Join("\n", failisisu));

            #region Loen sisse read ja kui rida on vigane...
            List<string> vigasedread = new List<string>();
            List<Tulemus> tulemused = new List<Tulemus>();
            for (int i = 1; i < failisisu.Length; i++)
            {
                try
                {
                    var osad = failisisu[i].Split(',');
                    tulemused.Add(new Tulemus { Nimi = osad[0], Aeg = double.Parse(osad[2]), Distants = double.Parse(osad[1]) });
                }
                catch (Exception e)
                {
                    vigasedread.Add($"{i + 1}. {failisisu[i]} # {e.Message}");
                }
            }
            vigasedread.ForEach(x => Console.WriteLine(x));
        }
    } 
    #endregion

    class Tulemus
        {
            public string Nimi { get; set; }
            public double Distants { get; set; }
            public double Aeg { get; set; }

            public double Kiirus => Distants / Aeg;
        }

        }




//try
//{
//    string filename = @"C:\Users\marin_m3sdpn5\Source\Repos\second-week-mar2020\SpordipaevTeisiti\Sport.txt"; //loen faili sisse
//    var sisu = File.ReadAllLines(filename);//loe kõik read sisse

//    Console.WriteLine(string.Join("\n", sisu)); //trükin välja

//}
//catch (Exception)
//{
//    Console.WriteLine("Kus fail on ? ");
//    //Console.WriteLine(e.Message);
//    throw;
//}
