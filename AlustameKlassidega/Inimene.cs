﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    class Inimene
    {
        //iga klass koosneb andmetest, baidid, bitid, nende tähendusest
        public string Nimi; //vaikimisi tühi string
        public int Vanus = 18; // vaikimisi 0 - zero
        public Inimene Kaasa; // vaikimisi kandilinenull - null

        //klass koosneb funtksionaalsusest - mis tehted selles klassis on
        //vaatame hiljem seda poolt täpsemalt
        public override string ToString() => $"Inimene {Nimi} vanusega {Vanus}";
    }
    
   
    }

