﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    class Program
    {
        static void Main(string[] args)
        {

            Inimene mari = new Inimene();
            mari.Nimi = "Mari Niinemäe";
            mari.Vanus = 28;

            Inimene ants = new Inimene() { Nimi = "Ants Saunamees", Vanus = 80 };

            List<Inimene> rahvas = new List<Inimene>
            {
            new Inimene {Nimi = "Joosep", Vanus = 118 },
            new Inimene {Nimi = "Teele" },
            new Inimene {Nimi = "Arno"},
            mari,  // paneme mari ja antsu listi
            ants,
            new Inimene{Nimi = "Anton", Kaasa = mari},
            };

            /* Klassi liikmed on:
             väljad
             funktsioonid
             meetodid
             propertid
             indexerid
             eventid
             operaatorid
             konstruktorid
             */
            // class (struct) on definitsioon
            // objekt on KONKTEETNE eksemplar, mis vastab sellele definitsioonile

            // Inimene - class
            // mari - object sellest klassist
            // classi definitsioon ütleb, millest seda tüüpi objekt koosneb - data (mudel)
            // ja mida temaga teha saab - funktsionaalsus

            // mudel on lihtsustatud kujutis asjast
            //klasside järjekord ei ole oluline
      


            Console.WriteLine(mari);

            MinuLoomad.Koer koer = new MinuLoomad.Koer();

            Inimene teine = mari; // omistamine muutuja ja avaldis sama tüüpi
            teine.Nimi = "Henn";

            Console.WriteLine(mari);

            Inimene[] inimesed = new Inimene[10]; // 10 inimest massiivis
            List<Inimene> teised = new List<Inimene>();

            teised.Add(new Inimene { Nimi = "Ants", Vanus = 28 });
            teised.Add(new Inimene { Nimi = "Peeter"});
            teised.Add(mari);
            mari.Kaasa = new Inimene {Nimi = "Anton", Kaasa = mari };

            // ? - tehteid vaatame kunagi hiljem 
            foreach (var x in teised) 
                Console.WriteLine($"{x} ja tema kaasa on {x.Kaasa?.Nimi?? "kaasa puudub"}");


        }
    }
    //definitsioonide järjekord ei oma tähtsust - class Inimene võib ka olla eespoolt kui class Program
    /*
     klass on defintisooon
               nimi
               andmetüüp
               muutuja tüübina
     jaotatakse namespacesse (nimeruumi) - et nimed ei läheks sassi

   a)  klassid võivad olla ühes failis mitu tükki 
   b)  igaüks oma failis 
   c)  kombineeritult

    muutuja                             väärtus
    classi tüüpi muutujal on sees oma väärtuse AADRESS
    muutuja omistamisel teisele kopeeritakse AADRESS

    Inimene teine = henn; // sellel omistamise kopeeritakse aadress

    struct erineb classist - selle poolest, et muutuja sees on väärtus
    omistamisel kopeeritakse väärtus

    class on reference tüüpi asi - muutujas on aadress
    struct on value tüüpi asi - muutujas on value/väärtus

  */

}
