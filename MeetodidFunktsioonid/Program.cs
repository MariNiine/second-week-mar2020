﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetodidFunktsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            #region esimene näiteplokk
            //// Console.BackgroundColor = ConsoleColor.Yellow; //teeb tausta kollaseks
            // DateTime sünnipäev = new DateTime ( 1992, 2, 21 );

            // Trüki(sünnipäev); // meetodi väljakutse

            // Console.WriteLine(Vanus(sünnipäev)); // funktsiooni kasutamine (avaldises)
            // if (Vanus(sünnipäev) > 18) Console.WriteLine("talle võib alkot müüa"); 
            #endregion

            Inimene mari = new Inimene { Nimi = "Mari", Sünniaeg = new DateTime(1992, 2, 21) };
            Inimene ants = new Inimene { Nimi = "Ants", Sünniaeg = DateTime.Parse("17.aprill.2000")};

            Console.WriteLine(mari);
            Console.WriteLine(ants);

            //Console.WriteLine(mari.AgeInstants());

            //Console.WriteLine(Inimene.AgeStatic(ants));

            List<Inimene> kahekesi = new List<Inimene>
            { 
            mari,ants,
            };
            foreach (var x in kahekesi) x.AgeInstants();

            for (int i = 0; i < kahekesi.Count; i++)
                Console.WriteLine(kahekesi[i].AgeInstants());
        }


        static void Trüki(DateTime päev) // meetod - programmiblokk, mis teeb midagi. Kutsume välja
        {
            Console.WriteLine(päev.ToString("(ddd) dd.MMMM.yyyy"));
        }

    }

    class Inimene // inimese klassi sees on erinevad väljad - public, private (ei paista välja)
    {
        static int inimesteArv = 0;
        static int adult = 18;

        public int Number = ++inimesteArv;
        public string Nimi;
        public DateTime Sünniaeg;

        public bool KasTäisealine() { return AgeInstants() > adult; }


        public override string ToString()
        => $"{Number}. {Nimi} kes on sündinud {Sünniaeg: dd.MMMM.yyyy}";


        // mari.AgeInstants();
        public int AgeInstants() => Vanus(this.Sünniaeg);

        // Inimene.AgeStatic(ants);
        public static int AgeStatic(Inimene kes) => Vanus(kes.Sünniaeg);


        static int Vanus(DateTime päev) // funktsioon - arvutab midagi. Kasutame avaldises
        {
            return (DateTime.Today - päev).Days * 4 / 1461;
            // annab tagasi vastuse, mida kasutatakse avaldises
        }
    }
}

/*
 1. public - igalt poolt kättesaadav
 2. protected - kättesaadav oma klassi ja sellest klassist tuletatud uute klasside sees
 3. internal - saadav programmisiseselt
 4. private - kättesaadav ainult selle klassi sees
 */

    // Kui ei ole öeldud, siis classi liikmed on private
    // class ja struct ise on vaikimisi internal