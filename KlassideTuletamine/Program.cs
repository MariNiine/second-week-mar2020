﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom loom1 = new Loom();
           // Console.WriteLine(loom1);
            Loom loom2 = new Loom("krokodill");
           // Console.WriteLine(loom2);

            Koduloom kl = new Koduloom("prussakas") { Nimi = "Albert" };
            // Console.WriteLine(kl);

            // loom1.TeeHaalt();
            // kl.TeeHaalt();

            List<Loom> loomaaed = new List<Loom> // tegin loomade listi
            {
            loom1,
            loom2,
            kl,
            new Kass {Nimi = "Miisu" },
            new Kass {Nimi = "Garfiled" }
            };


            //Kass k = (Kass)loomaaed[3];  // lugesin muutujasse
            //k.Silita();

            // ((Kass)loomaaed[3]).Silita(); 

            // kontrollin, kas loom on kass, siis ainult silitan
            if (loomaaed[2] is Kass) ((Kass)loomaaed[3]).Silita();
            if (loomaaed[2] is Kass k) k.Silita(); // identne eelmise lausega
            (loomaaed[2] as Kass)?.Silita();  // võta lommaaiast loom nr 2, vaata, kas on kass

            //loomaaed [2] is Kass? (Kass)loomaaed[2] : null)

            foreach (var x in loomaaed)
            {
                // Console.WriteLine(x);  // x on loom muutuja, sest loomaaed on list, mis koosneb loomadest. Kutsutakse välja klooma meetod
                (x as Kass)?.Silita();
                x.TeeHaalt();  
            }
            Sepik sepik = new Sepik();
            Koer pauka = new Koer { Nimi = "Pauka" };

            Lõuna(sepik);
            Lõuna(pauka);

            Koer[] koerad =
            {
            pauka,
            new Koer {Nimi = "Polla"},
            new Koer {Nimi = "Muki"},
            new Koer {Nimi = "Adalbert"},
            new Koer {Nimi = "Pontu"}
            };

            Array.Sort(koerad);
            

        }

        static void Lõuna(ISöödav x)
        {
            x.Süüakse();
        }

    }

    abstract class Elajas
        // abstraktselt klassil pole keha. Meetodit pole olemas, koht on kirjelduses olemas. 
    {
        public abstract void Söömine(Elajas e);
    }

    class Loom : Elajas
    { 
        public string Liik { get; private set; } // property
        public Loom (string liik) // konstruktor
        {
        Liik = liik;
        }
        public Loom() : this("tundamtu") // Sidumine C#
        { 
        }

        public virtual void TeeHaalt() // klassile tegin meetodi juurde
            //Virtuaalne meetod ja funtksioon saab tuletatud klassis ümber defineerida. Kui virtual ei kirjuta, siis kasutatakse baasklassi meetodit.
        {
            Console.WriteLine($"{Liik} teeb koledat häält");
        }
        public override string ToString() => $"Loom {Liik} liigist";

        public override void Söömine(Elajas e)
        {
            Console.WriteLine($"{this} sööb {e}");
        }
    }

    class Koduloom : Loom // ühest klassist teise tuletamine. Kõik, mis on olemas loomal, on ka koduloomal. Baasklassist ei tule kaasa konstruktorid.
    {
        public string Nimi { get; set; }
        public Koduloom (string liik) : base(liik) // Baasklassi konstruktori võid välja kutsuda. Sidumine baasklassi konstrukotriga
        {
            // Konstruktor peab igal klassil olema oma. See baasklassist kaasa ei tule
        }

        public override void TeeHaalt() 
        {
            // base.TeeHaalt(); see tuli vaimimisi
            Console.WriteLine($"{Liik} {Nimi} häälitseb mõnusalt");
        }
        public override string ToString() => $"{Liik} {Nimi}";

    }

    class Kass : Koduloom  // sealed klassi ees muudab klassi tuletamisel kasutatamatuks
    {
        private bool tuju = false;
        public string Tõug { get; set; }
        public Kass() : base("kass") { }

        public void Silita() => tuju = true;
        public void SikutaSabast() => tuju = false;

        public override void TeeHaalt()
        {
            Console.WriteLine(
                tuju ? $"{Nimi} lööb nurrru" : $"{Nimi} kräunub"
                );
        }

    }

    class Koer : Koduloom, ISöödav
    {
        public string Tõug { get; set; }
        public Koer() : base("koer") { }
        public void Süüakse()
        {
        Console.WriteLine($"Koer {Nimi} pannakse nahka");
        }

        public void JäetakseSöömata()
        {
            throw new NotImplementedException();
        }

        public bool KasMaitses()
        {
            throw new NotImplementedException();
        }

        public int CompareTo(object obj)
        {
            return obj is Koer k ? Nimi.CompareTo(k.Nimi) : 1;
        }
    }

    interface ISöödav  
    {
        void Süüakse();
        void JäetakseSöömata();
        bool KasMaitses();
    }

    class Sepik : ISöödav
    {
        public void JäetakseSöömata()
        {
            throw new NotImplementedException();
        }

        public bool KasMaitses()
        {
            throw new NotImplementedException();
        }

        public void Süüakse()
        {
            Console.WriteLine("Keegi nosib sepikut");
        }
    }

}
