﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torutehted
{

    static class E
    {
        //public static IEnumerable<int> Paaris(this IEnumerable<int> m) // kui ette kirjutada this, saan teda välja kutsuda . (punkt) ja meetod
        //{
        //    foreach (var x in m)
        //        if (x % 2 == 0) yield return x;
        //}

        //public static IEnumerable<int> Ruudud(this IEnumerable<int> m)
        //{
        //    foreach (var x in m)
        //        yield return x * x;
        //        }

        //public static IEnumerable<int> Suuremad(this IEnumerable<int> m, int v)
        //{
        //    foreach (var x in m)
        //        if (x > v) yield return x;
        //}

        //public static IEnumerable<int> Kas(this IEnumerable<int> m, Func<int, bool> f)
        //{
        //    foreach (var x in m) if (f(x)) yield return x;
        //}

        //public static IEnumerable<int> Arvuta(this IEnumerable<int> m, Func<int, int> f)
        //{
        //    foreach (var x in m) yield return f(x);
        //}
    }
    class Program
    {

        //  static bool KasOnPaaris(int x) => x % 2 == 0;
        //  static bool KasOnSuurem(int x, int v) => x > v;

        static void Main(string[] args)
        {
            int[] arvud = { 1, 7, 2, 4, 3, 1, 9, 6, 2, 8, 10, 7 };
            foreach (var x in arvud
                 // .Suuremad(5)
                 // .Paaris()
                 .Where((int x) => x % 2 == 0)
                 .Where(x => x > 5)
                 .Select(x => x * x)

               ) Console.WriteLine(x);

            string[] nimed = { "Mari", "Henn", "Peeter", "Joosep", "Jakob" };
            foreach (var n in nimed
                .Where(x => x[0] == 'J')
                ) Console.WriteLine(n);
             

            foreach (var x in from y in arvud
                              where y > 5 && y % 2 == 0
                              select y * y)

                Console.WriteLine(x);


            Random r = new Random();

            Console.WriteLine("\n Lambdad spordipäeval\n");
            foreach (var x in
                System.IO.File.ReadAllLines(@"C:\Users\marin_m3sdpn5\source\repos\second-week-mar2020\Torutehted\Spordipaevaprotokoll.txt")
                .Skip(1) // .Skip  .Take
                         // .Take(10)
                         // .Where(x => r.Next(5) == 3)

                .Select(x => x.Split(','))
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = double.Parse(x[2]) })
                .Where(x => x.Aeg > 0)
                .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants / x.Aeg })
                //.OrderBy(x => x.Kiirus)
                .GroupBy(x => x.Distants)
                .Select(x => new {Distants = x.Key, Parim = x.OrderByDescending(y => y.Kiirus).First()})


                ) Console.WriteLine(x);
        }
    }
}

// Lambda avaldise üldkuju
// millest => mida
// (int x, int y) => x * y;
// (x, y) => x * y;

