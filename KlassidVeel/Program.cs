﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidVeel
{
    enum Mast { Risti = 1, Ruutu, Ärtu, Poti }
    [Flags]
    enum Tunnus {Suur = 1, Punane = 2, Puust = 4, Kolisev = 8}

    static class Program // static class on selline, mille sees ei saa olla midagi peale staticu
    {
        static void Main()
        {
           Tunnus t = Tunnus.Suur | Tunnus.Kolisev;
            t |= Tunnus.Punane;
            Console.WriteLine(t);
            Console.WriteLine((Tunnus)7);

            Mast m = Mast.Risti;
            m++;
            Console.WriteLine((int)m);
            Console.WriteLine((Mast)3);

            Inimene mari = new Inimene { Nimi = "mari niinemäe", Palk = 8000 };
            Console.WriteLine(mari);

            mari.SetPalk(10000);
            Console.WriteLine(mari.GetPlak());
            mari.SetPalk(5000);
            Console.WriteLine(mari.GetPlak());

            Console.WriteLine(mari.Palk);
            mari.Palk += mari.Palk + 500;

           
        }

        static void VanaMain()
        {
            Console.WriteLine(Inimene.ToProper("mari"));
            Console.WriteLine(Inimene.ToProper(""));
            Console.WriteLine(Inimene.ToProper("ees nimi"));
        }
        // vana main
        static void MainEelmine(string[] args)
        {

            int a = 7;
            int b = a * a;


            Inimene mari = new Inimene { Nimi = "Mari Niinemäe", IK = "49202215216"};
         //   Console.WriteLine(mari);

            Inimene ants = new Inimene { Nimi = "vana Ants" };
            //    Console.WriteLine(ants.Sünniaeg());

            ants = null;
            ants = new Inimene { Nimi = "Noor Ants" }; // sellel hetekl vana ants UNUSTATAKSE

            foreach (var i in Inimene.Inimesed) Console.WriteLine(i);
            Console.WriteLine(Liida(100)); // y = 0 JA z = 0
            Console.WriteLine(Liida(4, 7)); // positsioonilised argumendid z = 0
            Console.WriteLine(Liida(x : 4, z : 7)); // nimelised argumendid y = 0
           
            Console.WriteLine(Liidax(1));
            Console.WriteLine(Liidax(1,2,3,4,5,6,7));

            //using (Inimene ajutine = new Inimene { Nimi = "Keegi" })
            //{ 
            ////selles blokis on inimene olemas

            //} // siit alates seda muutujat enam pole
        }

        static int Liida(int x, int y = 0, int z = 0)
        { 
            return 4*x + 2*y + z; 
        }

        static int Liidax(int x, params int[] muud)

        {
            int sum = x;
            foreach (var s in muud) sum += s;
            return sum;
        }

        #region Tegime halva overloadimise näite
        //static int Liida(int x, int y, int z) => x + y + z; // sama mis return {}
        //static double Liida(double d1, double d2)
        //{ return d1 + d2 +100 ; }
        //static double Liida(int d1, double d2)
        //{ return d1 + d2 + 200; }
        //static double Liida(double d1, int d2)
        //{ return d1 + d2 + 300; }  
        #endregion


        // overload - sama nimega funktsiion/meetod erinevad parameetrid (tüüp, arv, järjekord)
        // optioonal - defineeritud väärtusega parameeter (võib pöördumise puududa)
        // params array - muutuv arv parameetreid (käsitletakse massiivina)

        // positsiooniline pöördumine liida(4,7,8)
        // nimeline pöördumine liida (z : 8, x:4, y:7)
    }
}

#region Värvide tähendused
// võtmesõnad - sinised või lillad (ploki algused ja lõpud)
// andmetüübid, klassi nimed - helesinised
// stringid - punased
// kommentaarid - rohelised
// meetodite, funtksioonide nimed - pruunid
// spetsiaalmärgid stringi sees - helelilla \n 
#endregion


#region JÄRJEKORD
/*
1. väljad
  static
  object

2. construktorid
  static/objektid

3. funktsioonid/meetodid
  static
  object
*/
#endregion