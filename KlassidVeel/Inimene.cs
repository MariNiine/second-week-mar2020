﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidVeel
{
    

// view ja class view navigeerimiseks mugav

    class Inimene
    {

        static int loendur = 0; // viisakas on näidata algväärtust
        static List<Inimene> inimesed = new List<Inimene>();
        public static IEnumerable<Inimene> Inimesed => inimesed.AsEnumerable();

        private string _Nimi = "";  // muutsime nime välja kontrollituks
        public string Nimi 
        { 
            get => _Nimi;

            set => _Nimi = ToProper(value);
         }


        public string IK = "10101010000"; // vaikimis kõigil asjadel mingi väärtus
       
        public int nr { get; } = ++loendur;

        

        private decimal _Palk;  

        public decimal GetPlak()  // funktsioon getter
        {
            return _Palk;
        }

        public void SetPalk(decimal palk) => _Palk = palk > _Palk ? palk : _Palk; // meetod setter
                                                                                  //{
                                                                                  //    if (palk > _Palk) _Palk = palk;
                                                                                  //}

            // PROPERTY - get, set paar, mis näeb välja nagu filed (väli)
            // võimaldab kontrollida ja kontollitut muuta private väljasid
        public decimal Palk
        {
            get => _Palk;
            set => _Palk = value > _Palk ? value : _Palk;
         }

        public Inimene()  // voidi pole, klassi nimega sama nimi - selle asja nimi on konstruktor
            // CONSTRUKTOR
            //majapidamistoimingut uue objekti loomisel
            // klassi nimega seotud meetod
            //käivitatakse automaatselt new ajal

        {

            this.Nimi = "";
            this.IK = "48202215216";

            inimesed.Add(this);
        }


        #region Kommentaarid
        // klassil ja objektil on elütsükkel
        // inimene i = new inimene (); kui enne pole inimesega midagi ette võetud, siis loetakse alguses mällu see klass
        // selle käigus saavad väärtuse staatilised väljad
        // kui täidetakse lause new - saavad väärtuse objekti ( mittestaatilised) väljad
        // kui täidetakse uuesti new - tekib uus objekt 
        #endregion


        public DateTime Sünniaeg()
        {
            return
               (IK.Length < 11) ? new DateTime() :

                new DateTime(
           (IK[0] == '1' || IK[0] == '2' ? 1800 :
                IK[0] == '1' || IK[0] == '4' ? 1900 : 2000) +
                int.Parse(IK.Substring(1, 2)),
                int.Parse(IK.Substring(3, 2)),
                int.Parse(IK.Substring(5, 2))
                );
        }

        public static string ToProper(string nimi)
        {
            // kuidas teha üks nimi suure algustähega
            // kuidas saada esitäht, kuidas saada ülejäänud tähed

            // nüüd oleks vaja mitme nimega sama asja teha
            // nimi tükeldada ja iga tükk teha suureks
            // pärast vaja uuesti kokku panna
            // kuidas - tükeldada, kokku panna
            //split - string.join

            var nimed = nimi.Split(' '); // nii sai tükeldada
            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed [i] == "" ? "" : // nüüd peaks tühja nimega ka toimima
                nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1).ToLower();
            }

            return string.Join(" ", nimed); // nii saab kokku panna 

          
          
            // proovime 
        }

        public override string ToString()
        {
            return $" {nr}.{Nimi} sündinud {Sünniaeg(): dd.MMMM.yyyy}";

        }
    }
}


