﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vahepala
{
    class Program
    {
        static void Main(string[] args)
        {

           // Palgaarvestus.Maksuvaba = 1000;
           // Console.WriteLine(Palgaarvestus.Maks(800));

            List<Inimene> inimesed = new List<Inimene>
            {
            new Inimene {Nimi = "Mari", Palk = 11111 },
            new Inimene {Nimi = "Ants", Palk = 8000 } ,
            new Inimene {Nimi = "Mari", Palk = 100 },
            };
            foreach (var i in inimesed) Console.WriteLine($"{i.Nimi} saab {i.Palk - i.Tulumaks()} raha ");
        }
    }

    class Inimene
    {

        //need asjad on kõigil ühised
        public static decimal Maksuvaba { get; set; } = 500;
        public static decimal Maksumäär{ get; set; } = 0.2M;

        //public string Nimi { get; set; }
        //public decimal Palk { get; set; }
        //public decimal Tulumaks => Palk < Maksuvaba ? 0 : (Palk - Maksuvaba) * Maksumäär;

        public static decimal Maks(decimal summa) => summa < Maksuvaba ? 0 : (summa - Maksuvaba) * Maksumäär;
 

        // need asjad on kõigil oma
        public string Nimi { get; set; }
        public decimal Palk { get; set; }
        public decimal Tulumaks() => Maks(Palk);
    }
}
