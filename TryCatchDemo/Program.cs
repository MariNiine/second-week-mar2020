﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchDemo
{
    class Program2
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Ma tahaks jagada");

            try
            {
                Console.Write("Anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());

                Console.Write("Anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());

                if (teine == 0) throw new Exception("Nulliga ma ei oska jagada");
                int tulemus = yks / teine;

                Console.WriteLine($" Tulemus {tulemus}");
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR");
                Console.WriteLine(e.Message);
                throw; 
            }
            finally
            {
                Console.WriteLine("\nTänaseks lõpetame jagamise");
            }
        }
    }
}

