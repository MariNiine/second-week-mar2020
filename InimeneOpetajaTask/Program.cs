﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace InimeneOpetajaTask
{



    class Program
    {
        static void Main(string[] args)
        {

            //Console.WriteLine(new Inimene { Nimi = "Mari Niine", Isikukood = "49202215216" });
            // Console.WriteLine(new Opilane { Nimi = "Henn Sarv", Isikukood = "48202215216", Klass = "7C" });
            //Console.WriteLine(new Opetaja { Nimi = "Malle Maasikas", Isikukood = "44202215216", Aine = "matemaatika" });
            // Console.WriteLine(new Opetaja { Nimi = "Tiit Tuvi", Isikukood = "34202215216", Aine = "füüsika", Klass = "7C" });


            #region Teen inimeste listi
            List<Inimene> Koolipere = new List<Inimene>
            {
                new Inimene {Nimi = "Mari Niine", Isikukood = "47202215216" },
                new Opilane { Nimi = "Henn Sarv", Isikukood = "48202215216", Klass = "7C" },
                new Opetaja { Nimi = "Malle Maasikas", Isikukood = "44202215216", Aine = "matemaatika" },
                new Opetaja { Nimi = "Tiit Tuvi", Isikukood = "34202215216", Aine = "füüsika", Klass = "7C" },
                };
            #endregion

            #region Loen failid sisse ja määran, mis millises tulbas on
            var õpsid = File.ReadAllLines(@"C:\Users\marin_m3sdpn5\Source\Repos\second-week-mar2020\OpilasedOpetajad\Opetajad.txt");
            foreach (var õ in õpsid)
            {
                string[] osad = (õ + ",,").Split(',');
                Koolipere.Add(new Opetaja { Isikukood = osad[0], Nimi = osad[1].Trim(), Aine = osad[2], Klass = osad[3].Trim() });
            }

            õpsid = File.ReadAllLines(@"C:\Users\marin_m3sdpn5\Source\Repos\second-week-mar2020\OpilasedOpetajad\Opilased.txt");
            foreach (var õ in õpsid)
            {
                string[] osad = (õ + ",,").Split(',');
                Koolipere.Add(new Opilane { Isikukood = osad[0], Nimi = osad[1].Trim(), Klass = osad[2].Trim() });
            } 
            #endregion

            // kirjutan iga inimese kohta rea
            foreach (var x in Koolipere) Console.WriteLine(x);
        }

        enum sugu { Naine, Mees }


        class Inimene
        {
            public string Nimi { get; set; }
            public string Isikukood { get; set; } = "";
            public sugu Sugu => (sugu)(Isikukood[0] % 2);

            public override string ToString() => $"{Sugu} {Nimi} ({Isikukood})";
        }

        class Opilane : Inimene
        {
            public string Klass { get; set; }

            public override string ToString() => $"{Klass} klassi õpilane {Nimi} ({Isikukood})";
        }

        class Opetaja : Inimene
        {
            public string Aine { get; set; }
            public string Klass { get; set; } = "";
            public override string ToString() => $"{Aine} õpetaja {Nimi} ({Isikukood})" +
               ( Klass == "" ? "" : $" / {Klass} klassi juhataja");
        }
    }

}