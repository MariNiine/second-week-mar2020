﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpilasedOpetajad
{

    enum sugu { Naine, Mees }
    enum LapseSugu { Tüdruk, Poiss }

    class Inimene
    {
        public string Nimi { get; set; }
        public string Isikukood { get; set; } = "";
        public int adult = 20;
        public sugu Sugu => (sugu)(Isikukood[0] % 2);
        public LapseSugu LapseSugu => (LapseSugu)(Isikukood[0] % 2);

        public string SuguStr => Vanus >= 20 ? Sugu.ToString() : LapseSugu.ToString();

        public DateTime Sünniaeg =>
           Isikukood == "" ? new DateTime() : 
            new DateTime (

               (Isikukood[0] == '5' || Isikukood[0] == '6' ? 2000 :
                Isikukood[0] == '3' || Isikukood[0] == '4' ? 1900 : 
                1800) +
                int.Parse(Isikukood.Substring(1, 2)), // siia aasta
                int.Parse(Isikukood.Substring(3, 2)), // siia kuu
                int.Parse(Isikukood.Substring(5, 2))  // siia päev
                );

        public int Vanus => (DateTime.Today - Sünniaeg).Days * 4 / 1461;   // 1461 = (365 *4 + 1)

        public int VanusO() => (DateTime.Today - this.Sünniaeg).Days * 4 / 1461;   // 1461 = (365 *4 + 1)
        public static int VanusC (Inimene p) => (DateTime.Today - p.Sünniaeg).Days* 4 / 1461;


        public override string ToString()
         => $"{SuguStr} {Nimi} on {Vanus} aastane";
        };
    }




