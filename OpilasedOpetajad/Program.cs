﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace OpilasedOpetajad
{
    static class MyFunuctions
    {
        public static int Vanus2 (this Inimene p) => (DateTime.Today - p.Sünniaeg).Days * 4 / 1461; // A

        public static string[] ReadAllLines(this string filename) // B
            => System.IO.File.ReadAllLines(filename);

        public static int Liida(int a, int b) // C
        {
            return a + b;
        }
        public static void Tryki(int a, string nimi) 
        {
            Console.WriteLine($"{nimi}: {a}");
        }
        public static void Arvuta(int x, int y, out int summa, out int korrutis) //D
        {summa = x + y;
         korrutis = x * y;
        }
        public static (int, int) Arvuta(int x, int y)
        { return (x + y, x * y); 
        }

        public static void Vaheta(ref int x, ref int y)  // E
        {
            //int ajutine = x;
            //x = y;
            //y = ajutine;
            (x, y) = ( y, x);
        }

        //public static void Vaheta (ref string x, ref string y) => (x,y) = (y,x); // vahetab stringe
        public static void Swap<T>(ref T x, ref T y) => (x, y) = (y, x); // oskab vahetada kõike F
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyFunuctions.Tryki(MyFunuctions.Liida(7, 4), "Tulemus"); //C

            int s, k;
            MyFunuctions.Arvuta(7, 5, out s, out k); //D
            MyFunuctions.Swap(ref s, ref k);  // F

            MyFunuctions.Vaheta(ref k, ref s); // E

            DateTime minuSünnipäev = new DateTime(1992, 2, 21); // F
            DateTime täna = DateTime.Today;
            MyFunuctions.Swap(ref minuSünnipäev, ref täna);


            #region Inimeste katsed
            Inimene mari = new Inimene { Isikukood = "60202215216", Nimi = "Mari" };
            //Console.WriteLine(mari);
            //Inimene juku = new Inimene { Isikukood = "51804010001", Nimi = "Juku" };
            //Console.WriteLine(juku); 
            #endregion
            Console.WriteLine(MyFunuctions.Vanus2(mari));
            Console.WriteLine(mari.Vanus2()); // A

            #region ÕPILASTE ÕPETAJATE ÜLESANNE
            List<Inimene> Inimesed = new List<Inimene>();

            ReadInimesed(@"C:\Users\marin_m3sdpn5\Source\Repos\second-week-mar2020\OpilasedOpetajad\Opetajad.txt", Inimesed);
            ReadInimesed(@"C:\Users\marin_m3sdpn5\Source\Repos\second-week-mar2020\OpilasedOpetajad\Opilased.txt", Inimesed);

            Console.WriteLine("\nJärmise kuu sünnipäevalapsed: \n");
            foreach (var p in Inimesed)
                if (p.Sünniaeg.Month == DateTime.Now.Month + 1) //
                    Console.WriteLine(p);

            DateTime startNextWeek = DateTime.Today.AddDays(7 - ((int)(DateTime.Today.DayOfWeek) + 6) % 7);
            DateTime stopNextWeek = startNextWeek.AddDays(7);

            Console.WriteLine("\nJärgmise nädala sünnipäevalapsed: \n");
            foreach (var p in Inimesed)
            {
                DateTime sünnipäev = p.Sünniaeg.AddYears(p.Vanus + 1);
                if (sünnipäev >= startNextWeek && sünnipäev < stopNextWeek) Console.WriteLine(p);

            }
        }
        static void ReadInimesed(string filename, List<Inimene> list)
        {
            foreach (var rida in File.ReadAllLines(filename)) // B tänu sellele saab nii kirjutada - in filename.ReadAllLines()
            {
                var osad = rida.Split(',');
                if (osad.Length > 1)
                    list.Add(new Inimene { Isikukood = osad[0].Trim(), Nimi = osad[1].Trim() });
            } 
            #endregion
        }
    }
}



 
    


